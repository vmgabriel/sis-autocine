import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Usuario generado con los datos necesarios en mongoose
**/
const usuarioModel = new Schema({
    cedula: Number,
    nombres: String,
    apellidos: String,
    correo: String,
    pass: String,
    puntos: Number
});

export default mongoose.model('usuario', usuarioModel);
