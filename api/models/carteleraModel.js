import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Cartelera generado con los datos necesarios en mongoose
**/
const carteleraModel = new Schema({
    fechaReservaInicio: { type: Date, default: Date.now },
    fechaReservaFin: { type: Date },
    pelicula: { type: Schema.Types.ObjectId, ref: 'pelicula' },
    sala: { type: Schema.Types.ObjectId, ref: 'sala' }
});

export default mongoose.model('cartelera', carteleraModel);
