import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Factura de Pagos generado con los datos necesarios en mongoose
**/
const facturaPagosModel = new Schema({
    nit: Number,
    fechaCompra: { type: Date, default: Date.now },
    fechaReservaFin: { type: Date },
    cliente: { type: Schema.Types.ObjectId, ref: 'usuario' },
    facturaSnacks: { type: Schema.Types.ObjectId, ref: 'tiqueteSnack' },
    boletas: [{ type: Schema.Types.ObjectId, ref: 'boleta' }]
});

export default mongoose.model('facturaPago', facturaPagosModel);
