import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Punto Agil generado con los datos necesarios en mongoose
**/
const puntoAgilModel = new Schema({
    fechaActualizacion: { type: Date, default: Date.now },
    direccion: String
});

export default mongoose.model('puntoAgil', puntoAgilModel);
