import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Item generado con los datos necesarios en mongoose
**/
const itemModel = new Schema({
    nombre: String,
    marca: String,
    descripcion: String,
    precio: Number,
    tamano: String
});

export default mongoose.model('item', itemModel);
