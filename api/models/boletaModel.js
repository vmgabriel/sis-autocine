import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Boleta generado con los datos necesarios en mongoose
 **/
const boletaModel = new Schema({
    fechaReserva: { type: Date, default: Date.now },
    iscompra: Boolean,
    author: { type: Schema.Types.ObjectId, ref: 'Silla' }
});

export default mongoose.model('boleta', boletaModel);
