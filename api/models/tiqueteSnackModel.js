import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase TiqueteSnacks generado con los datos necesarios en mongoose
**/
const tiqueteSnacksModel = new Schema({
    fechaCompra: { type: Date, default: Date.now },
    snacks: [{ type: Schema.Types.ObjectId, ref: 'snack' }]
});

export default mongoose.model('tiqueteSnack', tiqueteSnacksModel);
