import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Pelicula generado con los datos necesarios en mongoose
**/
const peliculaModel = new Schema({
    nombre: String,
    director: [String],
    guionista: [String],
    fechaEstreno: { type: Date, default: Date.now },
    costoDerechos: Number,
    descripcion: String,
    categoria: String,
    generos: [{ type: Schema.Types.ObjectId, ref: 'genero' }]
});

export default mongoose.model('pelicula', peliculaModel);
