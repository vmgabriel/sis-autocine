import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Silla generado con los datos necesarios en mongoose
**/
const sillaModel = new Schema({
    estado: String,
    tipo: String,
    sala: { type: Schema.Types.ObjectId, ref: 'sala' },
});

export default mongoose.model('silla', sillaModel);
