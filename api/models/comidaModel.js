import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Comida generado con los datos necesarios en mongoose
**/
const comidaModel = new Schema({
    item: {type: Schema.Types.ObjectId, ref: 'item'},
    salsas: [{type: Schema.Types.ObjectId, ref: 'salsa'}]
});

export default mongoose.model('comida', comidaModel);
