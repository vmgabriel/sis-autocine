import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Salsa generado con los datos necesarios en mongoose
**/
const salsaModel = new Schema({
    nombre: String,
    descripcion: String
});

export default mongoose.model('salsa', salsaModel);
