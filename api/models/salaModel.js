import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Sala generado con los datos necesarios en mongoose
**/
const salaModel = new Schema({
    numero: Number,
    tamano: Number,
    multiplex: { type: Schema.Types.ObjectId, ref: 'multiplex' },
});

export default mongoose.model('sala', salaModel);
