import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Multiplex generado con los datos necesarios en mongoose
**/
const multiplexModel = new Schema({
    nombre: String,
    direccion: String
});

export default mongoose.model('multiplex', multiplexModel);
