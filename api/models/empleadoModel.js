import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Empleado generado con los datos necesarios en mongoose
**/
const empleadoModel = new Schema({
    cedula: Number,
    nombres: String,
    apellidos: String,
    telefono: Number,
    fechaInicioContrato: { type: Date, default: Date.now },
    salario: Number,
    multiplex: { type: Schema.Types.ObjectId, ref: 'multiplex' }
});

export default mongoose.model('empleado', empleadoModel);
