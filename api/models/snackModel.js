import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Snack generado con los datos necesarios en mongoose
**/
const snackModel = new Schema({
    item: { type: Schema.Types.ObjectId, ref: 'item' },
});

export default mongoose.model('snack', snackModel);
