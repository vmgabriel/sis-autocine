import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
   @class La clase Genero generado con los datos necesarios en mongoose
**/
const generoModel = new Schema({
    nombre: String,
    descripcion: String,
    clasificacion: String
});

export default mongoose.model('genero', generoModel);
