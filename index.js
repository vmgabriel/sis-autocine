// Archivo Base

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const mongoose = require('mongoose');

const configDB = require('./config/db');

const app = express();

const port = 3000;
let options = {
    server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } }
};


app.set('dbUrl', configDB.db[app.settings.env]);
// connect mongoose to the mongo dbUrl
mongoose.connect(app.get('dbUrl'), { useNewUrlParser: true });

app.use(bodyParser.json());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json'}));

app.listen(port);


app.get("/", (req, res) => res.json({message: "API de CINE DISTRITO - Compras en Linea"}));


console.log('todo list RESTful API server started on: ' + port);

module.exports = app; // for testing
