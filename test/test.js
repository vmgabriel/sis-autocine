
process.env.NODE_ENV = 'test';

const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();

describe('Mocha', () => {
    describe('Funcionalidad', () => {
        it('Deberia retornar -1 cuando el valor no este en el arreglo, uso de mocha', () => {
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });
});


chai.use(chaiHttp);

describe('pagina /', () => {
    describe('La Pagina esta respondiendo', () => {
        it("Esta debe decir pagina API", (done) => {
            chai.request(server)
                .get('/')
                .end((err, res) =>{
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.to.have.property('message', "API de CINE DISTRITO - Compras en Linea");
                    done();
                });
        });
    });
});


// Uso de Mongo para revisar la Conexion con la base de datos

describe('Mongo', () => {
    describe('Conection', () => {
        it ('La Conexion debe estar establecida correctamente', () => {
            const mongoose = require('mongoose');
            mongoose.connect('mongodb://db-test:27017/test', { useNewUrlParser: true }).then(
                () => {assert.equal(true,false);},
                err => {assert.equal(true,false);}
            );
        });
    });
});
