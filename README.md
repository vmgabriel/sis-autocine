# Universidad Distrital Francisco José de Caldas
## Facultad Ingenieria

### Integrantes
- Nicolas Vargas
- Gabriel Vargas

### Roles
- Juan Nicolás Vargas -> Líder
- Gabriel Vargas -> Líder de desarrollo
- Juan Nicolás Vargas -> Líder de planeación
- Juan Nicolás Vargas -> Líder de Calidad
- Gabriel Vargas -> Lider de Soporte

### Descripcion del Problema
Cine Distrito, es una empresa dedicada a la proyección de películas en las diferentes salas que tienen en Latinoamérica. Ud. y su equipo han sido contratados para la construcción del sistema compra en línea, el cual permitirá que los usuarios vendan tanto boletería como snacks desde puntos ágiles que pueden ser localizados en locales cerca a los diferentes multiplex, o en el mismo multiplex. 
En éste momento, Cine Distrito cuenta con los multiplex de Titán, Unicentro, Plaza Central, Gran Estación, Embajador (centro), y Las Américas.

Cada punto ágil consta de un cliente (Web) y una base de datos local, la cual tiene información fragmentada y además, debe poder sincronizar cierta información (que ustedes como equipo, deben identificar). 
Los multiplex tienen mínimo 5 salas, máximo 15 y cada sala tiene 40 sillas en general y 20 en preferencial. Las sillas generales tienen un precio de $11.000 y las de preferencial $15.000. Además cada multiplex vende hot dogs, sándwiches, nachos, pop corn (sal y dulce o mixtos), chocolatinas, gaseosa, etc. 
Los compradores al llegar a un punto ágil pueden seleccionar el multiplex en el cual quieren ver una película. También podrán ver por sala la disposición de las sillas y la disponibilidad de cada una. Además, podrá comprar snacks y también podrá realizar el pago de su consumo, pues si no hay pago, la silla seleccionada vuelve a quedar disponible para que otro usuario la use. 
Además, se debe poder identificar los puntos adquiridos por cada cliente, pues con cada compra de boletas, se obtienen 10 ptos, y por cada compra de snacks, se obtienen 5 ptos. Cuando un comprador obtiene 100 ptos, Cine Distrito le regala una boleta en general para ver la película que el usuario elija dentro de los 6 meses siguientes a la obtención de los 100 ptos. 
Los usuarios de los puntos ágiles son empleados de Cine Distrito y tienen un rol y un cargo fijo único en un único multiplex (los cargos pueden ser: director, cajero, despachador de comida, encargado de sala, aseador), que puede ser cambiado cada 3 meses. 
El personal contratado por la empresa se identifica mediante un código de empleado que mantendrán mientras trabajen en Cine Distrito independientemente del multiplex a la que 
estén asignados. La Administración almacena para cada empleado la cédula, el nombre, el número de teléfono, la fecha de comienzo de contrato, el salario y el multiplex en el que trabaja. Cada empleado sólo puede estar asignado a una franquicia. 
Además, en el multiplex de Titán, en Bogotá, se elaboran estudios estadísticos acerca de la movilidad de los empleados de la empresa, para lo cual necesitan sus datos de fecha de inicio de contrato y de salario. 
La base de datos centralizada alimenta un sistema que genera reportes de las operaciones mensuales. Como Cine Distrito aún no ha definido éstos reportes, se pide al equipo de desarrollo que construya 2 reportes de prueba con las operaciones más importantes generadas en la empresa. 

Se pide: PRIMERA ENTREGA:

1. Diseño centralizado Bases de Datos. 
  - Producto a entregar: Modelo Relacional 
2. Diseño del proceso 
  - Producto a entregar: diagrama de procesos (BPMN) 
3. Identificar los sitios de distribución (sedes) 
  - Producto a entregar: lista de sedes 
4. Analizar qué distribuir (tablas a fragmentar) 
  - Producto a entregar: definición de los fragmentos. 
5. Fragmentación 
  - Producto a entregar: esquema de fragmentación. 
6. Esquema de asignación 
  - Producto a entregar: lista de sedes con los correspondientes fragmentos por sede. 
7. Diseño de arquitectura de solución. 
  - Producto a entregar: Diagrama de despliegue. 
  - Producto a entregar: Especificación de lenguaje, plataforma, frameworks, etc. 
  - Producto a entregar: Diagrama de componentes. 
  - Producto a entregar: Casos de uso (especificación) 
8. Sistemas que se operarán en cada franquicia (se ejecutará la prueba con mínimo 2) 
9. IMPORTANTE!! Si se escoge la opción de Middleware que permita sincronizar la información, este DEBE ser construido por uds.!! 
10. Sistema FUNCIONANDO! Justificar las decisiones tomadas en cada paso 

# Diagrama de Clases
![Diagrama de Clases](docs/DiagramaClases.png "Diagrama de Clases 0.1")

# Construccion de la Aplicacion

Uso necesario de docker-compose

```bash
$ docker-compose up -d
```

# Generacion de Documentacion

Para con respecto a la generacion documental se tiene jsdoc quien va a hacer el proceso de construccion

```bash
$ grunt jsdoc
$ xdg-open docs/index.html
```
