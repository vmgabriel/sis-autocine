FROM node:8

WORKDIR /home/node/app

COPY package*.json ./

RUN npm install nodemon -g

COPY . ./

RUN npm install

EXPOSE 3000
CMD ["nodemon", "."]